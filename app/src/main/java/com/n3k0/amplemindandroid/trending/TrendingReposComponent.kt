package com.n3k0.amplemindandroid.trending

import com.n3k0.amplemindandroid.di.ScreenScope
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ScreenScope
@Subcomponent
interface TrendingReposComponent: AndroidInjector<TrendingReposController>{

    @Subcomponent.Builder
    abstract class Builder: AndroidInjector.Builder<TrendingReposController>()
}