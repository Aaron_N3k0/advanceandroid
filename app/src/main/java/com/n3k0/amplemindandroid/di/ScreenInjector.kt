package com.n3k0.amplemindandroid.di

import android.app.Activity
import com.bluelinelabs.conductor.Controller
import com.n3k0.amplemindandroid.base.BaseActivity
import com.n3k0.amplemindandroid.base.Basecontroller
import dagger.android.AndroidInjector
import java.util.HashMap
import javax.inject.Inject
import javax.inject.Provider

@ActivityScope
class ScreenInjector @Inject constructor(
        private val screenInjectors: Map<Class<out Controller>, Provider<AndroidInjector.Factory<out Controller>>>
) {
    private val cache = HashMap<String, AndroidInjector<Controller>>()

    fun inject(controller: Controller) {
        if (controller !is Basecontroller) {
            throw IllegalArgumentException("Controller must extend BaseController")
        }

        val instanceId = controller.getInstanceId()
        if (cache.containsKey(instanceId)) {
            cache.get(instanceId)?.inject(controller)
            return
        }

        val injectorFactory = screenInjectors[controller.javaClass]?.get() as AndroidInjector.Factory<Controller>
        val injector = injectorFactory.create(controller)
        cache[instanceId] = injector
        injector.inject(controller)
    }

    fun clear(controller: Controller) {
        cache.remove(controller.instanceId)
    }

    companion object {
        fun get(activity: Activity?): ScreenInjector {
            if (activity !is BaseActivity) {
                throw IllegalArgumentException("Controller must be hosted bu BaseActivity")
            }

            return activity.screenInjector
        }
    }
}