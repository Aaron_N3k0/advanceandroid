package com.n3k0.amplemindandroid.di

import com.bluelinelabs.conductor.Controller

import dagger.MapKey
import kotlin.reflect.KClass

@MapKey
@kotlin.annotation.Target(
        AnnotationTarget.FUNCTION,
        AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER)
annotation class ControllerKey(val value: KClass<out Controller>)
