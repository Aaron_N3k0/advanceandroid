package com.n3k0.amplemindandroid.di

import android.app.Activity
import android.content.Context
import com.n3k0.amplemindandroid.base.BaseActivity
import com.n3k0.amplemindandroid.base.MyApplication
import dagger.android.AndroidInjector
import java.util.HashMap
import javax.inject.Inject
import javax.inject.Provider

class ActivityInjector @Inject constructor(
        val activityInjectors: Map<Class<out Activity>, Provider<AndroidInjector.Factory<out Activity>>>
) {

    private val cache = HashMap<String, AndroidInjector<out Activity>>()

    fun inject(activity: Activity) {
        if (activity !is BaseActivity) {
            throw IllegalArgumentException("Activity must extend BaseActivity")
        }

        val instanceId = activity.getInstanceId()
        if (cache.containsKey(instanceId)) {
            (cache[instanceId] as AndroidInjector<Activity>).inject(activity)
            return
        }

        val injectorFactory = activityInjectors[activity.javaClass]?.get() as AndroidInjector.Factory<Activity>
        val injector = injectorFactory.create(activity)
        cache[instanceId!!] = injector
        injector.inject(activity)
    }

    fun clear(activity: Activity) {
        if (activity !is BaseActivity) {
            throw IllegalArgumentException("Activity must extend BaseActivity")
        }
        cache.remove(activity.getInstanceId())
    }

    companion object {
        fun get(context: Context): ActivityInjector {
            return (context.applicationContext as MyApplication).activityInjector
        }
    }

}