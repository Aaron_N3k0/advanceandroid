package com.n3k0.amplemindandroid.home

import com.bluelinelabs.conductor.Controller
import com.n3k0.amplemindandroid.di.ControllerKey
import com.n3k0.amplemindandroid.trending.TrendingReposComponent
import com.n3k0.amplemindandroid.trending.TrendingReposController
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

@Module(subcomponents = [
    TrendingReposComponent::class
])
abstract class MainScreenBindingModule {

    @Binds
    @IntoMap
    @ControllerKey(TrendingReposController::class)
    abstract fun bindTrendingReposInjection(builder: TrendingReposComponent.Builder): AndroidInjector.Factory<out Controller>
}