package com.n3k0.amplemindandroid.home

import com.n3k0.amplemindandroid.R
import com.n3k0.amplemindandroid.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun layoutRes(): Int {
        return R.layout.activity_main
    }
}
