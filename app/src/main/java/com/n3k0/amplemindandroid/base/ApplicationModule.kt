package com.n3k0.amplemindandroid.base

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private var application: Application) {

    @Provides
    fun provideApplicationContext(): Context {
        return application
    }
}