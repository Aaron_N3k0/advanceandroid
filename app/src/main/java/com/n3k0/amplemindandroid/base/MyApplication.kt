package com.n3k0.amplemindandroid.base

import android.app.Application
import com.n3k0.amplemindandroid.di.ActivityInjector
import javax.inject.Inject

class MyApplication : Application() {

    @Inject lateinit var activityInjector: ActivityInjector

    private lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
        component.inject(this)
    }
}