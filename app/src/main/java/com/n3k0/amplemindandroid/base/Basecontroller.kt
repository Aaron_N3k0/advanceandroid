package com.n3k0.amplemindandroid.base

import android.content.Context
import com.bluelinelabs.conductor.Controller
import com.n3k0.amplemindandroid.di.Injector

abstract class Basecontroller: Controller() {

    private var injected = false

    override fun onContextAvailable(context: Context) {
        // Controller instances are retained acroos config changes,
        // so this method can be called more than once
        // this makes sure we don't waste any time injecting more than once,
        // though technically it wouldn't change functionality
        if(!injected){
            Injector.inject(this)
            injected = true
        }
        super.onContextAvailable(context)
    }
}